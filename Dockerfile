FROM python:3.9-slim-buster


ENV PYTHONUNBUFFER=1
RUN mkdir /core
WORKDIR /core
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .